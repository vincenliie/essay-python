import os
import numpy as np
import cv2 as cv
import time

from CNN import CNN

img_size = 96

#Functions

def introduce():
  now = time.time()
  print("""\nWelcome to Sistem Pengenalan Sidik Jari\nCreated by Vincenlie Triadi (c) 2020. All Rights Reserved.\nTime : %s """ % time.strftime("%a %b %d %Y %I:%M:%S %p", time.localtime()),
  end="\n\n"
  )
def main():
  Real_path   = "Dataset/Testing"
  Easy_path   = "Dataset/Training/Easy"
  Medium_path = "Dataset/Training/Medium"
  Hard_path   = "Dataset/Training/Hard"

  introduce()

  cnn = CNN(num_classes=10)
  cnn.setModel()  
  cnn.assign_input([Easy_path])
  
  print()
  print("========== TRAIN ==========")
  print()
  cnn.train(epoch=30, write_model_path="Model/Train2.vt")
  # cnn.create_confusion_matrix_calculate_accuracy()
if __name__ == "__main__":
  main()
