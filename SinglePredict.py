import numpy as np
# import matplotlib.pyplot as plt
import  cv2 as cv

from tkinter import *
from tkinter.ttk import *
from PIL import ImageTk
from tkinter import filedialog

class SinglePredict(Toplevel):

  def __init__(self, cnn):
    super().__init__()
    self.title("Single Prediction")
    self.path = ""
    self.input = None
    self.cnn = cnn
    self.geometry("400x300")

    self.initUI()

  def initUI(self):
    # self.master = master
    # self.pack(fill=tk.BOTH, expand=1)
    self.config(bg="#333")
    # Style().configure("TFrame", background="#333")

    self.lblPath = Label(self, text="Path : ", background="#333", foreground="#fff")
    self.lblPath.place(x=10, y=10, width=80)

    self.txtPath = Entry(self)
    self.txtPath.place(x=80, y=10, width=180)

    self.lblResult = Label(self, text="Result : ", background="#333", foreground="#fff")
    self.lblResult.place(x=10, y=40, width=80)

    self.txtResult = Entry(self)
    self.txtResult.place(x=80, y=40, width=200)

    self.lblPhoto = Label(self, text="Photo")
    self.lblPhoto.place(x=290, y=10, width=100, height=107)

    self.btnLoad = Button(self, text="...", command= lambda: self.getFile())
    self.btnLoad.place(x=260, y=10, width=20, height=20)
    
    self.btnPredict = Button(self, text="Predict", command= lambda: self.predict())
    self.btnPredict.place(x=200, y=70, width=80)

  def getFile(self):
    self.path = filedialog.askopenfilename(initialdir = "Dataset",title = "Select file",filetypes = (("BMP files","*.BMP"),("all files","*.*")))
    
    if self.path :
      img = ImageTk.PhotoImage(file=self.path)

      self.lblPhoto.configure(image=img)
      self.lblPhoto.image = img
      
      self.txtPath.insert(0, self.path)
      
      input = cv.imread(self.path, cv.IMREAD_GRAYSCALE)
      input = cv.resize(input, (96,96))
      ret,input = cv.threshold(input,127,255,cv.THRESH_BINARY)

      input = np.array(input).reshape(1,96,96,1)
      input = input/255

      self.input = input

      print("Image Loaded !")
      print(self.input.shape)

  def predict(self):
    # self.cnn.summary()
      prediction, time = self.cnn.predict_single(self.input)
      # print(prediction)
      # print(np.array(prediction).shape)
      self.txtResult.delete(0, END)
      if time > 1000:
        self.txtResult.insert(0, str(np.argmax(prediction))+" - "+str(round(time/1000,2))+" s.")
      else:
        self.txtResult.insert(0, str(np.argmax(prediction))+" - "+str(time)+" ms.")

    