

class Homepage :

  def make_greeting(self):
    return "<h1>Hello, Master</h1>"

  def make_greeting_someone(self, name):
    return "<h1>Hello, "+name+"</h1>"

  def make_greeting_gender(self, name, gender):
    if gender == "male":
      return "<h1>Hello, Mr. "+name+"</h1>"
    else:
      return "<h1>Hello, Ms. "+name+"</h1>"
  
  def homepage():
    pass
