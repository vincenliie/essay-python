# Convolution
      # def forward(self):
    #   kernel_size, kernel_size, total_input, total_output = self.kernel.shape
    #   # self.depth, self.height, self.width = self.inputLayer.shape

    #   output = []
    #   for i in range(0, total_output):
    #     sementara = []
    #     for j in range(0, total_input):  
    #       inp_height = []
    #       for k in range(-1, self.height - 1):
    #         inp_width = []
    #         for l in range(-1, self.width - 1):
    #           total = 0
    #           for m in range(0, kernel_size):
    #             for n in range(0, kernel_size):
    #               if (k + m) < 0 or k + m > self.height - 1 or l + n > self.width - 1 or (l + n) < 0:
    #                 total += 0
    #               else:
    #                 total += self.inputLayer[j][k+m][l+n] * self.kernel[m][n][j][i]

    #           inp_width.append(total)
    #         inp_height.append(inp_width)
    #       sementara.append(inp_height)
    #     output.append(self.merge(sementara)+self.bias[i])

    #   if self.activation != None:
    #     if self.activation == 'relu':
    #       output = self.relu_forward(output)

    #   self.outputLayer = output

    #   return np.array(output)

  # def read_network_model_json(self, path):
  #     model = []
  #     with open(path, "r") as model_file:
  #         data = js.load(model_file)
  #         self.accuracy = data['accuracy']
  #         for p in data['model']:
  #             types = p['Layer']['type']
  #             ps = p['Layer']
  #             if types == 'Convolution':
  #                 model.append(ConvolutionLayer(
  #                     (ps['height'], ps['width']), ps['depth']))
  #             elif types == 'Cross Entropy':
  #                 model.append(CrossEntropy(ps['class']))
  #             elif types == 'Flatten':
  #                 model.append(FlattenLayer(
  #                     (ps['width'], ps['height']), ps['depth']))
  #             elif types == 'FullyConnected':
  #                 model.append(FullyConnectedLayer(
  #                     ps['output'], ps['input']))
  #             elif types == 'Pooling':
  #                 model.append(PoolingLayer(
  #                     (ps['width'], ps['height'], ps['depth']), stride=ps['stride'], kernel_size=ps['kernelSize']))
  #             elif types == 'Relu':
  #                 model.append(ReluLayer())
  #             elif types == 'Softmax':
  #                 model.append(SoftmaxLayer(classes=ps['class']))
  #             else:
  #                 continue

  #         for p in data['kernels']:
  #             ps = p['kernel']
  #             model[ps['layer_num']].setKernel(ps['value'])

  #         for q in data['weights']:
  #             qs = q['weight']
  #             model[qs['layer_num']].setWeight(qs['value'])

  #         self.Model.clear()
  #         self.Model = model

  def write_network_model_json(self, path):
    data = {}
    convos = []
    fcls = []
    data['accuracy'] = self.accuracy
    data['model'] = []
    data['kernels'] = []
    data['weights'] = []

    l = 0
    for i in self.Model:
        if type(i) == ConvolutionLayer:
            convos.append(l)
            data['model'].append({
                'Layer':
                {
                    'type': 'Convolution',
                    'width': i.width,
                    'height': i.height,
                    'depth': i.depth
                }})
        elif type(i) == CrossEntropy:
            data['model'].append({
                'Layer': {
                    'type': 'Cross Entropy',
                    'class': i.num_classes
                }
            })
        elif type(i) == FlattenLayer:
            data['model'].append({
                'Layer': {
                    'type': "Flatten",
                    'width': i.width,
                    'height': i.height,
                    'depth': i.depth
                }})
        elif type(i) == FullyConnectedLayer:
            fcls.append(l)
            data['model'].append({
                'Layer': {
                    'type': "FullyConnected",
                    'input': i.height,
                    'output': i.width
                }})
        elif type(i) == PoolingLayer:
            data['model'].append({'Layer': {
                'type': 'Pooling',
                'height': i.height,
                'width': i.width,
                'depth': i.depth,
                'stride': i.stride,
                'kernelSize': i.kernel_size,
            }})
        elif type(i) == ReluLayer:
            data['model'].append({'Layer': {
                'type': 'Relu'
            }})
        elif type(i) == SoftmaxLayer:
            data['model'].append({'Layer': {
                'type': 'Softmax',
                'class': i.classes
            }})
        else:
            data['model'].append({'Layer': {
                'type': 'unidentified'
            }})

    with open(path, "w") as file_out:
        js.dump(data, file_out)
    