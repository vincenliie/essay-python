import os
import numpy as np
import cv2 as cv

from CNN import CNN

#Constants
img_size = 96

#Functions
def extract_label(path, train = True):
  filename, _ = os.path.splitext(os.path.basename(path))

  subject_id, etc = filename.split('__')
  
  if train:
      gender, lr, finger, _, _ = etc.split('_')
  else:
      gender, lr, finger, _ = etc.split('_')
  
  gender = 0 if gender == 'M' else 1

  if finger == 'thumb':
    if lr == 'Left':
      finger = 0
    else:
      finger = 1
  elif finger == 'index':
    if lr == 'Left':
      finger = 2
    else:
      finger = 3
  elif finger == 'middle':
    if lr == 'Left':
      finger = 4
    else:
      finger = 5
  elif finger == 'ring':
    if lr == 'Left':
      finger = 6
    else:
      finger = 7
  elif finger == 'little':
    if lr == 'Left':
      finger = 8
    else:
      finger = 9
      
  return np.array([subject_id, gender, finger], dtype=np.uint16)

def loading_data(path,train):
  print("loading data from: ",path)
  data = []
  for img in os.listdir(path):
    try:
      img_array = cv.imread(os.path.join(path, img), cv.IMREAD_GRAYSCALE)
      img_resize = cv.resize(img_array, (img_size, img_size)) 
      # ret,img_resize = cv.threshold(img_resize,127,255,cv.THRESH_BINARY)
      label = extract_label(os.path.join(path, img),train)
      data.append([label[0], img_resize ])
    except Exception as e:
      raise e
  return data

def unique(x):
  result = []
  for i in x:
    if i not in result:
      result.append(i)
  return result

def generate_target(list, num_classes = 2):
  if len(unique(list)) == num_classes :
    result = np.zeros((num_classes, num_classes))
    for i in range (0, num_classes):
      result[i][i] = 1
  else:
    raise ("Number of classes have different with the list")
  return result

def main():
  Real_path   = "Dataset/Testing"
  Easy_path   = "Dataset/Training/Easy"
  Medium_path = "Dataset/Training/Medium"
  Hard_path   = "Dataset/Training/Hard"

  Easy_data   = loading_data(Easy_path, train = True)
  Medium_data = loading_data(Medium_path, train = True)
  Hard_data   = loading_data(Hard_path, train = True)
  test        = loading_data(Real_path, train = False)

  data = np.concatenate([Easy_data, Medium_data, Hard_data], axis=0)

  train_data, train_label = [], []

  for label, feature in data:
    train_data.append(feature)
    train_label.append(label-1)

  train_data = np.array(train_data).reshape(-1, img_size, img_size)
  train_data = train_data / 255.0

  train_target = generate_target(train_label, num_classes = 50)

# for i in range(0, len(train_data)):
  inputJaringan = train_data[0]
  inputJaringan = np.array(inputJaringan).reshape(1, img_size, img_size)
  
  cnn = CNN()
  cnn.setModel_2()
  prediction_error = cnn.forward_2(inputJaringan, train_target[train_label[0]])

  print("Prediction Error : ", prediction_error)


def model_summary():
  cnn = CNN()
  cnn.setModel_2()

  print(cnn)

if __name__ == "__main__":
  # main()
  model_summary()
