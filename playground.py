import sys
import os
import time
from termcolor import colored

def generate_equalto( inp, steps=20):
  str = ''
  if inp<0 :
    for i in range(0, int(steps/2)):
      str += " "
  else:
    for i in range(0, min(inp, int(steps/2))):
      str += "="
    
    for i in range(int(steps/2), min(inp, int(steps/2)),-1):
      str += " "
  
  return str

def render_percentage(i):
  if i < 10:
    # return round(i, 3)
    return """%0.3f""" % i
  else:
    return """%0.2f""" % i

def render(i, total, steps=20, now=""):
  n = i/(total/steps)
  print('['+generate_equalto(int(n), steps=steps)+" "+str(render_percentage(i/total*100))+"% "+generate_equalto(int(n-(steps/2)), steps=steps)+"] "+ str(i) + "/"+str(total)+" Elapsed : "+render_time(int(time.time()-now)), end=" \r")

def render_no_time(i, total, steps=20):
  n = i/(total/steps)
  print('['+generate_equalto(int(n), steps=steps)+" "+str(round(i/total*100, 2))+"% "+generate_equalto(int(n-(steps/2)), steps=steps)+"] "+ str(i) + "/"+str(total), end=" \r")

def render_end_pb(total, steps=20, now=""):
  print('['+generate_equalto(int(steps/2),steps)+' 100.0% '+generate_equalto(int(steps/2),steps)+'] '+str(total)+'/'+str(total)+" Elapsed : "+render_time(int(time.time() - now)))

def render_end_pb_kbi(i, total, steps=20, now=""):
  n = i/(total/steps)
  print('['+generate_equalto(int(n), steps=steps)+" "+str(render_percentage(i/total*100))+"% "+generate_equalto(int(n-(steps/2)), steps=steps)+"] "+ str(i) + "/"+str(total)+" Elapsed : "+render_time(int(time.time()-now)), end=" \r")

def render_end_pb_no_time(total, steps=20):
  print('['+generate_equalto(int(steps/2),steps)+' 100.0% '+generate_equalto(int(steps/2),steps)+'] '+str(total)+'/'+str(total))

def add_z(tim):
  if tim<10:
    t = '0' + str(int(tim))
  else:
    t = int(tim)
  return str(t)


def render_time(time):
  if time >3600:
    h = round(time/3600, 0)
    m = round(time%3600/60, 0)
    s = round(time%3600%60, 0)

    return add_z(h)+":"+add_z(m)+":"+add_z(s)
  elif time > 60:
    m = round(time/60, 0);
    s = round(time%60, 0);
    return add_z(m)+":"+add_z(s)
  else:
    return "00:"+add_z(time)
    

def generate_progressbar(total, steps = 20):
  if(steps%2 == 0):
    now = time.time()
    for i in range(0, total):
      # print(''+str(i) + '/100', end=" \r")
      try:
        render(i,total, steps, now)
        time.sleep(0.1)
      except (KeyboardInterrupt, SystemExit):
        render_end_pb_kbi(i, total, 30, time.time())
        sys.exit(0)
    render_end_pb(total, steps, (time.time() - now))
  else:
    print("Cannot Render Progress Bar. Steps must be Even Number.")

def main():
  total = 1946
  generate_progressbar(total, 30)


if __name__ == "__main__" :
  main()
