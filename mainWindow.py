#!/usr/bin/env python3

"""
ZetCode Tkinter tutorial

In self script, we lay out images
using absolute positioning.

Author: Jan Bodnar
Website: www.zetcode.com
"""

# from PIL import Image, ImageTk
from tkinter import *
from tkinter import messagebox, scrolledtext, filedialog
from tkinter.ttk import Frame, Label, Style, Button, Entry, Scrollbar
from PIL import ImageTk
from CNN import CNN
from resultWindow import resultWindow
from SinglePredict import SinglePredict

import matplotlib.pyplot as plt
import numpy as np

import os
import numpy as np

class mainWindow(Frame):

  cnn = None

  def __init__(self, master = None):

    self.cnn = None
    self.modelPath = None
    self.test_dataset = None

    super().__init__(master=master)
    self.master.geometry("550x500+685+165")
    self.master = master
    self.initUI()

  def initUI(self):

    self.master.title("Klasifikasi Sidik Jari")
    self.master.resizable(False, False)
    self.master.resizable(width = False, height = False)
    self.pack(fill=BOTH, expand=1)

    Style().configure("TFrame", background="#999")

    self.lblEasy = Label(self, text="Easy : ", background="#999", foreground="#000")
    self.lblEasy.place(x=20, y=20, width=100)
    
    self.txtEasy = Entry()
    self.txtEasy.place(x=80, y=20, width=450)
    self.txtEasy.insert(0, "Dataset/Training/Easy")

    self.lblMed = Label(self, text="Medium : ", background="#999", foreground="#000")
    self.lblMed.place(x=20, y=60, width=100)
    
    self.txtMed = Entry()
    self.txtMed.place(x=80, y=60, width=450)
    self.txtMed.insert(0, "Dataset/Training/Medium")

    self.lblHard = Label(self, text="Hard : ", background="#999", foreground="#000")
    self.lblHard.place(x=20, y=100, width=100)
    
    self.txtHard = Entry()
    self.txtHard.place(x=80, y=100, width=450)
    self.txtHard.insert(0, "Dataset/Training/Hard")
    
    self.lblTest = Label(self, text="Testing : ", background="#999", foreground="#000")
    self.lblTest.place(x=20, y=140, width=100)
    
    self.txtTest = Entry()
    self.txtTest.place(x=80, y=140, width=430)
    self.txtTest.insert(0, "Select Testing Dataset...")
    self.txtTest.config(state="disabled")
    self.txtTest.config(foreground = '#a9a9a9')

    self.btnAskTest = Button(text="...", command=lambda : self.btnAskTestOnAction())
    self.btnAskTest.place(x=510, y=140, width=20, height=20)
    
    self.lblModel = Label(self, text="Model : ", background="#999", foreground="#000")
    self.lblModel.place(x=20, y=180, width=100)
    
    self.txtModel = Entry()
    self.txtModel.place(x=80, y=180, width=330)
    self.txtModel.insert(0, "Select Model Files...")
    self.txtModel.config(state="disabled")
    self.txtModel.config(foreground = '#a9a9a9')
    
    self.btnAskModel = Button(text="...", command=lambda : self.askModelFile())
    self.btnAskModel.place(x=410, y=180, width=20, height=20)
    
    self.lblLamp = Label(background="#ff0000")
    self.lblLamp.place(x=120, y=475, height=15, width=15)

    self.lblStatus = Label(text="Model is not Loaded", background="#999", foreground="#f00")
    self.lblStatus.place(x=150, y=475)
    
    self.lblLog = Label(self, text="Log : ", background="#999", foreground="#000")
    self.lblLog.place(x=10, y=240, width=100)

    self.txtLog = scrolledtext.ScrolledText(state = DISABLED)
    self.txtLog.place(x=10, y=260, width=530, height=200)
    self.txtLog.config(font=("consolas", 10), undo=True, wrap='word')

    self.btnModel = Button(text="Load Model", command=lambda : self.load_model())
    self.btnModel.place(x=450, y=177.5, width=80)

    self.btnTesting = Button(text="Testing", command=lambda : self.load_result())
    self.btnTesting.place(x=95.5, y=220, width=100)

    self.btnSingle = Button(text="Single Prediction", command=lambda : self.single_prediction())
    self.btnSingle.place(x=352.5, y=220, width=100)

  def btnAskTestOnAction(self):
    fd = filedialog.askdirectory(initialdir = "Dataset/", title = "Select Testing Dataset")
    if fd :
      self.setDisabledText(self.txtTest, fd)
      self.test_dataset = fd

  def setText(self, e, text):
    e.config(foreground="black")
    e.delete(0,END)
    e.insert(0,text)

  def setDisabledText(self, e, text):
    e.config(state="normal")
    e.config(foreground="black")
    e.delete(0,END)
    e.insert(0,text)
    e.config(state="disabled")

  def askModelFile(self):
    path = filedialog.askopenfilename(initialdir = "Model", title = "Select file", filetypes = (("VT Model files","*.vt"),("all files","*.*")))
    if path:
      self.modelPath = path
      self.setDisabledText(self.txtModel, path)

  def load_model(self):
    if self.modelPath:
      dir = self.modelPath
      path, file = os.path.split(dir)
      name, ext = file.split('.')

      print(name, ext)
      
      self.cnn = CNN()
    # self.cnn.assign_input([self.txtTest.get()], train=False)
      self.cnn.read_network_model_json(dir)
      self.insertLog(self.cnn.summary())

      self.lblLamp.configure({"background":"#0f0"})
      self.lblStatus.configure({"foreground":"#0f0"})
      self.lblStatus['text'] = "Model Loaded."
    else:
      messagebox.showerror("Model Path Not Found","Please make sure you insert the right model path.")
  
  def insertLog(self, text):
    self.txtLog.config(state=NORMAL)
    self.txtLog.insert(END, text+" \n")
    self.txtLog.config(state=DISABLED)

  def single_prediction(self):
    if self.cnn != None :
      SinglePredict(self.cnn)
    else:
      messagebox.showerror("Model Not Found", "Please make sure the model is loaded.")

  def load_result(self):
    if self.cnn != None :
      if self.test_dataset != None :
        self.cnn.assign_input([self.txtTest.get()], train=False)
        result, times = self.cnn.predict()
        expect = self.cnn.labels
        resultWindow(result, expect, times=times, cnn = self.cnn)
      else:
        messagebox.showerror("Dataset not Found", "Please make sure the Dataset is defined")
    else:
      messagebox.showerror("Model Not Found", "Please make sure the model is loaded.")

def main():
    root = Tk()
    # app = mainWindow()
    mainWindow(root)
    root.mainloop()


if __name__ == '__main__':
    main()