import numpy as np


class SoftmaxLayer:

    def __init__(self, num_classes=10):
        super().__init__()

        self.inputLayer = None
        self.outputLayer = None
        self.input_grad = None
        self.output_grad = None

        self.targetLayer = None

        self.classes = num_classes

    def __str__(self):
        return "Softmax Layer ("+str(self.classes)+")"

    def setInput(self, inp):
        self.inputLayer = inp

    def setInputGrad(self, inpGrad):
        self.input_grad = inpGrad

    def setLabel(self, label):
        self.targetLayer = label

    def setOutputGrad(self, inpGrad):
        self.output_grad = inpGrad

    # def forward(self):
    #   exp = []
    #   hasil = []
    #   for i in range (0, len(self.inputLayer)):
    #     exp.append(np.exp(self.inputLayer[i]))

    #   for i in range(0, len(exp)):
    #     hasil.append(exp[i]/np.sum(exp))

    #   self.outputLayer = hasil
    #   return hasil

    def forward(self):
      print("Softmax input :",self.inputLayer)
      exp = []
      hasil = []
      for i in range(0, len(self.inputLayer)):
          exp.append(np.exp(self.inputLayer[i]))

      for i in range(0, len(exp)):
          hasil.append(exp[i]/np.sum(exp))

      self.outputLayer = hasil
      print("Softmax Output : ", np.array(self.outputLayer))
      return hasil

    # public double backwardCNN(int expectedValue) {
    #       // PROCESS BACKWARD
    #       this.inputData.clearGradient();

    #       for (int i = 0; i < this.outputDepth; i++) {
    #           double indicator = (i == expectedValue) ? 1.0 : 0.0;
    #           double outputGradient = this.outputData.getWeight(i) - indicator;
    #           this.inputData.setGradient(i, outputGradient);
    #       }
    #       // RETURN NILAI ERROR

    #       return this.outputData.getWeight(expectedValue);
    #   }

    def backward(self):
        self.input_grad = None

        hasil = [self.output_grad[i] - self.targetLayer[i]
                for i in range(0, len(self.output_grad))]
        self.input_grad = hasil

        return hasil

        # hasil = []
        # exp = []
        # for i in self.output_grad:
        #     exp.append(np.exp(i))

        # for i in range(0, len(self.outputLayer)):
        #     hitung = exp[i]*(np.sum(exp)-exp[i])/(np.sum(exp)*np.sum(exp))
        #     hasil.append(hitung)
        # self.input_grad = hasil
        # return np.array(hasil)

    # def backward(self):
    #   self.input_grad = None

    #   hasil = []
    #   exp = []
    #   for i in self.output_grad:
    #     exp.append(np.exp(i))

    #   for i in range(0, len(self.outputLayer)):
    #     hitung = exp[i]*(np.sum(exp)-exp[i])/(np.sum(exp)*np.sum(exp))
    #     hasil.append(hitung)
    #   self.input_grad = hasil
    #   return np.array(hasil)
