import numpy as np
import tensorflow as tf

class PoolingLayer():

  def __init__(self, inputSize = (10, 10, 10), stride = 2, kernel_size = 2):
    self.stride = stride
    self.kernel_size = kernel_size
    self.height, self.width, self.depth = inputSize

    # Variables
    self.inputLayer = None
    self.outputLayer = None
    self.indexes = None

    self.output_grad = None
    self.input_grad = None
  
  def setInputGrad(self, inp):
    self.input_grad = inp

  def setOutputGrad(self, inp):
    self.output_grad = inp
  
  # def __str__(self):
  #   return "Pooling Layer (("+str(self.height)+" x "+str(self.width)+" x "+str(self.depth)+"), stride="+str(self.stride)+", kernel_size="+str(self.kernel_size)+")"
  def __str__(self):
    name = "Max Pooling Layer"
    input_sizing  = "("+str(self.height)+"x"+str(self.width)+"x"+str(self.depth)+")"
    output_sizing = "("+str(int(self.height/2))+"x"+str(int(self.width/2))+"x"+str(self.depth)+")"
    
    if len(name) > 20:
      value = name[0:20] + "\t\t" + input_sizing + "\t\t" + output_sizing
    else:
      value = name + "\t\t" + input_sizing + "\t\t" + output_sizing

    return value
  
  def setInput(self, inp):
    self.inputLayer = inp

  # def forward(self):
  #   hasil=[]
  #   index = []
  #   for a in range(0, len(self.inputLayer[0])):
  #     hasil_depth = []
  #     index_depth = []
  #     for i in range(0, len(self.inputLayer[0][0]), self.stride):
  #       index_row = []
  #       hasil_row = []
  #       for j in range(0, len(self.inputLayer[0][0][0]), self.stride):
  #         max=0;
  #         if i + self.stride-1 < len(self.inputLayer[0][0]) and j + self.stride-1 < len(self.inputLayer[0][0][0]) :
  #           for k in range (i, i+self.kernel_size):
  #             for l in range (j, j+self.kernel_size):
  #               if max == 0:
  #                 max = self.inputLayer[0][a][k][l]
  #               if self.inputLayer[0][a][k][l] >= max:
  #                 max = self.inputLayer[0][a][k][l]
  #                 indexX = l
  #                 indexY = k
  #           hasil_row.append(max)
  #           index_row.append([indexX, indexY])
  #       if hasil_row :
  #         hasil_depth.append(hasil_row)
  #         index_depth.append(index_row)
  #     hasil.append(hasil_depth)
  #     index.append(index_depth)
  #   self.outputLayer=[]
  #   self.outputLayer.append(hasil)

  #   print(np.array(hasil).shape)
  #   self.indexes = index
  #   print("Pooling :", np.array(self.outputLayer).shape)
  #   return self.outputLayer

  def forward(self):
    pool = tf.keras.layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='same')(self.inputLayer)

    self.outputLayer = pool.numpy()
    return pool.numpy()
  
  def backward(self):

    self.input_grad = None

    result = np.zeros((self.depth, self.height, self.width))
    # print(result.shape)
    #depth
    for a in range(0, len(self.inputLayer)):
      #vertical
      index_i = 0
      for i in range(0, self.height, 2):
        # horizontal
        index_j = 0
        for j in range (0, self.width, 2):
          #loop 1
          h,v = self.indexes[a][index_i][index_j]
          for k in range(i, i+2):
            #loop 2
            for l in range(j, j+2):
              if l == h and k == v :
                result[a][k][l] = self.output_grad[a][index_i][index_j]
          index_j+=1
        index_i+=1
    self.input_grad = result
    return result
  
  # def backward(self):
  #   result = np.zeros((self.depth, self.height, self.width))
  #   print("Result Shape : "+str(result.shape))
  #   print("Output_Grad Shape : "+str(np.array(self.output_grad).shape))
  #   # print(result.shape)
  #   #depth
  #   for a in range(0, len(self.output_grad)):
  #     #vertical
  #     index_i = 0
  #     for i in range(0, self.height, 2):
  #       # horizontal
  #       index_j = 0
  #       for j in range (0, self.width, 2):
  #         #loop 1
  #         h,v = self.indexes[a][index_i][index_j]
  #         for k in range(i, i+2):
  #           #loop 2
  #           for l in range(j, j+2):
  #             if l == h and k == v :
  #               result[a][k][l] = self.output_grad[a][index_i][index_j]
  #         index_j+=1
  #       index_i+=1
  #   self.input_grad = result
  #   return result
