import numpy as np

class ReluLayer:
  
  def __init__(self):
    self.inputLayer = None
    self.outputLayer = None
    self.output_grad = None
    self.input_grad = None

  def __str__(self):
    return "ReLU Layer"

  def setInput(self, inputLayer):
    self.inputLayer = inputLayer

  def setOutput(self, outputLayer):
    self.outputLayer = outputLayer

  def setInputGrad(self, inputLayer):
    self.input_grad = inputLayer

  def setOutputGrad(self, outputLayer):
    self.output_grad = outputLayer
  
  def forward(self):
    result = []
    for a in range(0, len(self.inputLayer)):
      hasil=[]
      for i in range(0, len(self.inputLayer[0])):
          hasil_row = []
          for j in range(0, len(self.inputLayer[0][0])):
              if(self.inputLayer[a][i][j]<0):
                  hasil_row.append(0)
              else:
                  hasil_row.append(self.inputLayer[a][i][j])
          hasil.append(hasil_row)
      result.append(hasil)
      self.outputLayer = result
    return self.outputLayer

  def backward(self):

    self.input_grad = None

    result = []
    for a in range(0, len(self.output_grad)):
      hasil=[]
      for i in range(0, len(self.output_grad[0])):
          hasil_row = []
          for j in range(0, len(self.output_grad[0][0])):
              if(self.output_grad[a][i][j]<=0):
                  hasil_row.append(0)
              else:
                  hasil_row.append(1)
          hasil.append(hasil_row)
      result.append(hasil)
      self.input_grad = result
    return self.input_grad
