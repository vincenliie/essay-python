import random
import numpy as np
import tensorflow as tf

class FullyConnectedLayer:

  # HEIGHT = INPUT

  def __init__(self, output_shape = 10, input_shape = 10, activation = "", weight_gen = False):
    self.width = output_shape[1]
    self.height = input_shape[1]

    if weight_gen :
      self.weight = self.generate_weight(input_shape[1], output_shape[1])
      self.bias = self.generate_bias(output_shape[1])

    self.learning_rate = .01

    self.inputLayer = None
    self.outputLayer = None

    self.input_grad = None
    self.output_grad = None
    self.weight_grad = None

    self.activation = activation

  def setBias(self, bias):
    self.bias = np.array(bias)
  
  def getBias(self):
    return self.bias
  
  def setInputGrad(self, inpGrad):
    self.input_grad = inpGrad

  def setOutputGrad(self, inpGrad):
    self.output_grad = inpGrad

  def setLearningRate(self, lr):
    self.learning_rate = lr

  def __str__(self):
    name = "Fully Connected Layer"
    input_sizing  = "("+str(self.height)+")\t"
    output_sizing = "("+str(self.width)+")"
    
    if len(name) > 20:
      value = name[0:20] + "\t\t" + input_sizing + "\t\t" + output_sizing
    else:
      value = name + "\t\t" + input_sizing + "\t\t" + output_sizing

    return value
    # return "Fully Connected Layer ( "+str(self.height)+" x "+str(self.width)+" )"

  # def forward(self):
  #   self.outputLayer = np.matmul(self.inputLayer, self.weight)
  #   self.outputLayer += self.bias
  #   return self.outputLayer

  def forward(self):
    dense = tf.keras.layers.Dense(self.width, activation='relu', weights=np.array([np.array(self.weight), np.array(self.bias)]))(self.inputLayer)
    
    self.outputLayer = dense.numpy()
    return dense.numpy()

  def backward(self):
    self.input_grad = None
    self.weight_grad = None

    # ref : http://cs231n.stanford.edu/handouts/linear-backprop.pdf (pg. 4)
    # reshape di gunakan untuk mengubah array 1 dimensi ke 2 dimensi
    igrad = np.dot(np.array(self.output_grad).reshape(1,-1), np.transpose(self.weight))
    self.input_grad = igrad.reshape(-1)

    # print("input grad fcl : "+ str(np.array(self.input_grad)))

    self.weight_grad = np.dot(np.transpose(np.array(self.inputLayer).reshape(1,-1)), np.array(self.output_grad).reshape(1,-1))

    assert np.array(self.weight_grad).shape == np.array(self.weight).shape

    weightlala = self.weight - (self.learning_rate * self.weight_grad)

    self.weight = weightlala.tolist()

    return self.input_grad

  def setInput(self, inp):
    self.inputLayer = inp

  def setWeight(self, inp):
    self.weight = np.array(inp)

  def getWeight(self):
    return self.weight

  def setOutput(self, outp):
    self.outputLayer = outp

  def generate_bias(self, size):
    hasil = []
    for i in range (0, size):
      hasil.append(random.uniform(0, 0.1))
    
    return np.array(hasil)

  def generate_weight(self, width, height):
    result = []
    for i in range(0, height):
      result_row = []
      for j in range(0, width):
        result_row.append(round(random.uniform(0, 0.01), 5))
      result.append(result_row)

    self.weight = np.array(result)
    return np.array(result)
