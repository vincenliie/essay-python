import numpy as np

class FlattenLayer:

  def __init__(self, inputSize):
    super().__init__()
    self.height, self.width, self.depth = inputSize


    self.inputLayer = None;
    self.outputLayer = None;

    self.input_grad = []
    self.output_grad = []

  def __str__(self):
    name = "Flatten Layer\t"
    input_sizing  = "("+str(self.height)+"x"+str(self.width)+"x"+str(self.depth)+")"
    output_sizing = "("+str(self.height*self.width*self.depth)+")"
    
    if len(name) > 20:
      value = name[0:20] + "\t\t" + input_sizing + "\t\t" + output_sizing
    else:
      value = name + "\t\t" + input_sizing + "\t\t" + output_sizing

    return value
    # return "FlattenLayer ("+str(self.height)+" x "+str(self.width)+" x "+str(self.depth)+" -> "+str(self.width*self.height*self.depth)+" )"

  def setInput(self, inp):
    self.inputLayer = inp;

  def setOutputGrad(self, inp):
    self.output_grad = inp;

  def setInputGrad(self, inp):
    self.input_grad = inp;
  
  def forward(self):
    result = []
    for a in range(0, len(self.inputLayer)):
      for i in range(0, len(self.inputLayer[0])):
          for j in range(0, len(self.inputLayer[0][0])):
              for k in range(0, len(self.inputLayer[0][0][0])):
                  result.append(self.inputLayer[0][i][j][k])
    result = np.array(result).reshape(1,-1)
    self.outputLayer = result
    return result

  def backward(self):

    self.input_grad = None

    reshaped = np.array(self.output_grad).reshape(-1, self.height, self.width)
    
    self.input_grad = reshaped

    return reshaped
