import os
import numpy as np

from tkinter import *
from tkinter import scrolledtext
from tkinter.ttk import Frame, Label, Style, Button, Entry, Scrollbar

class resultWindow(Toplevel):

  def __init__(self, result, exp, times, cnn):
    super().__init__()
    self.title("Result Window")
    # len_result = len(result)
    self.result = result
    self.exp = exp
    self.times = times

    self.cnn = cnn

    self.geometry("300x300")

    self.initUI()

  def initUI(self):
    # self.master = master
    # self.pack(fill=tk.BOTH, expand=1)
    self.config(bg="#333")
    # Style().configure("TFrame", background="#333")

    self.lblPranked = Label(self, text="Result : ", background="#333", foreground="#ffffff")
    self.lblPranked.place(x=10, y=10, width=150)

    self.txtResult = scrolledtext.ScrolledText(self, state = DISABLED)
    self.txtResult.place(x=10, y=40, width=280, height=250)
    self.txtResult.config(font=("consolas", 10), undo=True, wrap='word')
    self.txtResult.config(state=NORMAL)
    self.txtResult.insert(END, "Result : \n")
    self.txtResult.config(state=DISABLED)
<<<<<<< HEAD

    self.print_result()

  def insertLog(self, text = ""):
=======

    self.print_result()
  
  def insertLog(self, text):
>>>>>>> parent of f58b577 (latest sheyenk)
    self.txtResult.config(state=NORMAL)
    self.txtResult.insert(END, text+" \n")
    self.txtResult.config(state=DISABLED)
  
  def print_result(self):
    for i in range(0, len(self.result)):
<<<<<<< HEAD
      if self.times[i] < 1000:
        self.insertLog(text = str(i+1)+". "+str(self.exp[i])+"  ->  "+str(np.argmax(self.result[i]))+" - in "+str(int(self.times[i]))+" ms.")
      else:
        self.insertLog(text = str(i+1)+". "+str(self.exp[i])+"  ->  "+str(np.argmax(self.result[i]))+" - in "+str(round(self.times[i]/1000, 2))+" s.")
    self.insertLog()
    acc, recall, precision = self.cnn.create_confusion_matrix_calculate_accuracy()
    self.insertLog("Accuracy : "+ str(acc*100) + "%")
    self.insertLog("Recall : "+ str(recall))
    self.insertLog("Precision : "+ str(precision))
    self.insertLog(" ")
    self.insertLog("Max Classification Time : "+ str(round(np.max(self.times)/1000,2))+" s.")
    self.insertLog("Avg Classification Time : "+ str(round(np.average(self.times)/1000,2))+" s.")
    self.insertLog("Min Classification Time : "+ str(round(np.min(self.times)/1000,2))+" s.")

=======
      self.insertLog(text = str(i+1)+". "+str(self.exp[i])+"  ->  "+str(self.result[i]))
>>>>>>> parent of f58b577 (latest sheyenk)
  # def show(self):
    # root = tk.Tk()
    # self.initUI(master=root)
    # root.mainloop()