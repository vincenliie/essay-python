


class VGG16 :

  def __init__(self):
    setModel_16()

  def forward_16(self, inp, target):
    self.Model[36].setTarget(target)
    print(self.Model[36].targetLayer)
    conv_1 = []
    relu_1 = []
    self.Model[0].setInput(inp[0])
        
    print("Ukuran input Convo 1")
    print(np.array(inp).shape)
    print("Ukuran kernel Convo 1")
    print(np.array(self.Model[0].kernel).shape)
    for i in range(0, len(self.Model[0].kernel)):
        conv_1.append(convo.convolve2d(inp[0], np.flip(self.Model[0].kernel[i]), mode="same"))
    for i in range(0, len(self.Model[0].kernel)):    
        relu_1.append(self.relu(conv_1[i]))

    self.Model[0].setOutput(conv_1)
    self.Model[1].setInput(conv_1)
    self.Model[1].setOutput(relu_1)
    self.Model[2].setInput(relu_1)

    for l in range (2, len(self.Model)-1) :
      print("Input Layer "+str(l)+":")
      print(np.array(self.Model[l].inputLayer).shape)
      hasil = self.Model[l].forward()
      if l < len(self.Model):
        self.Model[l+1].setInput(hasil)
    
    self.Model[36].forward()
    print(self.Model[36].outputLayer)
    return self.Model[36].outputLayer
  # def imageInfo(self, image):

  def setModel_16(self):
    self.Model.append(ConvolutionLayer(64))    # 0
    self.Model.append(ReluLayer())             # 1
    self.Model.append(ConvolutionLayer(64))    # 2
    self.Model.append(ReluLayer())             # 3
    self.Model.append(PoolingLayer())          # 4

    self.Model.append(ConvolutionLayer(128))   # 5
    self.Model.append(ReluLayer())             # 6
    self.Model.append(ConvolutionLayer(128))   # 7
    self.Model.append(ReluLayer())             # 8
    self.Model.append(PoolingLayer())          # 9

    self.Model.append(ConvolutionLayer(256))   # 10
    self.Model.append(ReluLayer())             # 11
    self.Model.append(ConvolutionLayer(256))   # 12
    self.Model.append(ReluLayer())             # 13
    self.Model.append(ConvolutionLayer(256))   # 14
    self.Model.append(ReluLayer())             # 15
    self.Model.append(PoolingLayer())          # 16

    self.Model.append(ConvolutionLayer(512))   # 17
    self.Model.append(ReluLayer())             # 18
    self.Model.append(ConvolutionLayer(512))   # 19
    self.Model.append(ReluLayer())             # 20
    self.Model.append(ConvolutionLayer(512))   # 21
    self.Model.append(ReluLayer())             # 22
    self.Model.append(PoolingLayer())          # 23

    self.Model.append(ConvolutionLayer(512))   # 24
    self.Model.append(ReluLayer())             # 25
    self.Model.append(ConvolutionLayer(512))   # 26
    self.Model.append(ReluLayer())             # 27
    self.Model.append(ConvolutionLayer(512))   # 28
    self.Model.append(ReluLayer())             # 29
    self.Model.append(PoolingLayer())          # 30

    self.Model.append(FlattenLayer())          # 31 

    self.Model.append(FullyConnectedLayer(4096,4608))   # 32
    self.Model.append(FullyConnectedLayer(4096,4096))   # 33
    self.Model.append(FullyConnectedLayer( 5,4096 ))   # 34

    self.Model.append(SoftmaxLayer())                # 35
    self.Model.append(CrossEntropy())                # 36

    if Path('Model/model.txt').is_file():
      #Baca kernel dan Weights
      self.Model[0] .setKernel(self.read_kernel(1 ))
      self.Model[2] .setKernel(self.read_kernel(2 ))
      self.Model[5] .setKernel(self.read_kernel(3 ))
      self.Model[7] .setKernel(self.read_kernel(4 ))
      self.Model[10].setKernel(self.read_kernel(5 ))
      self.Model[12].setKernel(self.read_kernel(6 ))
      self.Model[14].setKernel(self.read_kernel(7 ))
      self.Model[17].setKernel(self.read_kernel(8 ))
      self.Model[19].setKernel(self.read_kernel(9 ))
      self.Model[21].setKernel(self.read_kernel(10))
      self.Model[24].setKernel(self.read_kernel(11))
      self.Model[26].setKernel(self.read_kernel(12))
      self.Model[28].setKernel(self.read_kernel(13))

    if Path('Model/model_fcl.txt').is_file():
      self.fcl_weight()
