import random
import os
import time
import numpy as np
import cv2 as cv
import scipy.signal as convo
import json as js

from pathlib import Path
from termcolor import colored

from Tools import ProgressBar
from Layers.ConvolutionLayer import ConvolutionLayer
from Layers.PoolingLayer import PoolingLayer
from Layers.ReluLayer import ReluLayer
from Layers.FullyConnectedLayer import FullyConnectedLayer
from Layers.FlattenLayer import FlattenLayer
from Layers.SoftmaxLayer import SoftmaxLayer
from Layers.CrossEntropy import CrossEntropy


class CNN:
    #     """
    #     A class that can build a CNN object

    #     Parameters:
    #     argument1 (int): Description of arg1

    #     Returns:
    #     int:Returning value

    #     """

    def __init__(self):
        # Inisialisasi Attribut
        self.inputLayer = None
        self.Model = []
        self.DEFAULT_IMAGE_SIZE = 96
        self.epoch = 0
        self.accuracy = 0
        self.labels = None
        self.learning_rate = 0.1
        self.history = {}
        self.inputFileNames = []
        # __init__
        # self.setModel()

    def add(self, object):
        self.Model.append(object)

    def summary(self):
      return_txt = ""

      print("_"*70)
      return_txt += "_"*70+"\n"
      print("="*70)
      return_txt += "="*70+"\n"
      print("Layer\t\t\t       Input Shape\t      Output Shape")
      return_txt += "Layer\t\t\t       Input Shape\t      Output Shape"+"\n"
      print("="*70)
      return_txt += "="*70+"\n"

      for i in range(0,len(self.Model)):
        print(str(self.Model[i]))
        return_txt += str(self.Model[i])+"\n"
        if i == len(self.Model):
          # print()
          print("="*70)
          return_txt += "="*70+"\n"
        else:
          return_txt += "_"*70+"\n"
          print("_"*70)
      
      return return_txt

    def unique(self, x):
        result = []
        for i in x:
            if i not in result:
                result.append(i)
        return result

    # proses merubah image yang diberikan sebagai input menjadi array
    def generate_img_arr(self, img):
        img_resize = cv.resize(img, (96, 96))
        np.array(img_resize).reshape(
            1, self.DEFAULT_IMAGE_SIZE, self.DEFAULT_IMAGE_SIZE)

        return img_resize

    # proses merubah image yang di load dari path menjadi sebuah array
    def process_img(self, path, img):
        try:
            img_array = cv.imread(os.path.join(path, img), cv.IMREAD_GRAYSCALE)
            img_resize = cv.resize(img_array, (96, 96))
            ret,img_resize = cv.threshold(img_resize,127,255,cv.THRESH_BINARY)
            np.array(img_resize).reshape(
                1, self.DEFAULT_IMAGE_SIZE, self.DEFAULT_IMAGE_SIZE,1)
        except Exception as e:
            raise e

        return img_resize

    # merubah tumpukan nama file menjadi label
    def process_labels(self, labels, train):
        lbls = []
        for i in labels:
            label = self.process_label(i, train)
            lbls.append(label)

        return lbls

    # mengubah sebuah nama file menjadi data penting yakni label pada CNN
    def process_label(self, filename, train):
        # folder, filename = os.path.splitext(filename)
        # filename, _ = filename.split('.')
        subject_id, etc = filename.split('__')

        if train:
            gender, lr, finger, _, _ = etc.split('_')
        else:
            gender, lr, finger, _ = etc.split('_')

        gender = 0 if gender == 'M' else 1

        if finger == 'thumb':
            if lr == 'Left':
                finger = 0
            else:
                finger = 1
        elif finger == 'index':
            if lr == 'Left':
                finger = 2
            else:
                finger = 3
        elif finger == 'middle':
            if lr == 'Left':
                finger = 4
            else:
                finger = 5
        elif finger == 'ring':
            if lr == 'Left':
                finger = 6
            else:
                finger = 7
        elif finger == 'little':
            if lr == 'Left':
                finger = 8
            else:
                finger = 9

        return np.array([subject_id, gender, finger], dtype=np.uint16)

    # loading dari path yang diberi dan merubah menjadi sebuah inputan OK
    def generate_inp(self, path, train):
        pb = ProgressBar()
        print("loading data from: ", path)
        data = []
        inputFileNames = os.listdir(path)
        self.inputFileNames.append(inputFileNames)
        total = len(self.inputFileNames)
        i = 0
        for img in os.listdir(path):
            pb.render_no_time(i, total)
            img_resize = self.process_img(path, img)
            label = self.process_label(img, train)
            data.append([label[0]-1, img_resize])
        pb.render_end_pb_no_time(total)

        self.width, self.height = 96, 96
        self.depth = len(data)
        return data

    def multipath_input_generator(self, path_arr, train=True):  # OK
      data = []
      labels = []
      self.inputFileNames = []
      for i in path_arr:
        inp = self.generate_inp(i, train)
        for label, feature in inp:
          data.append(feature)
        #   labels.append(self.generate_label(the_class=label))
          labels.append(label)
      print("Successfully created "+str(len(labels))+" input data.")
      return data, labels

    # input berupa image dan untuk memproses menjadi sebuah inputan
    def generate_inp_img(self, arr, labels, train=True): # OK
      data = []
      for img in arr:
        img_resize = self.generate_img_arr(img)
        label = self.proces_labels(labels, train)
        data.append([label[0], img_resize])

      return data

    # OK
    def train(self, epoch, write_model_path, patience=5, factor=0.005, min_lr=0.0001):
        accs = []
        self.epoch = epoch
        total_pict = len(self.inputLayer)
        pb = ProgressBar()
        counter = 0
        for j in range(0, epoch):
            now = time.time()

            if counter == patience:
                if self.learning_rate - factor > min_lr:
                    self.learning_rate -= factor
                print("Reducing Learning Rate to "+str(self.learning_rate))
                counter = 0

            print("Epoch "+str(j+1)+"/"+str(epoch)+" : ")

            for k in range(0, len(self.inputLayer)):
                error, prediction = self.forward(k)
                self.backward()
                pb.render(k, total_pict, error, now=now)

            pb.render_end_pb(total_pict, error, now=now)
            print("Epoch "+str(j+1)+" : " +
                  str(np.array(self.Model[7].kernel)), end="\r")
            print("Epoch "+str(j+1)+" : Start Validating Network...", end="\r")
            acc = self.create_confusion_matrix_calculate_accuracy()
            print("Epoch "+str(j+1)+" : Validation Finished. ", end="")
            print("Validated Accuraccy : "+str(round(acc*100, 2))+"%")
            accs.append(acc)
            if acc == self.accuracy:
                # Untuk patience
                counter += 1

            if acc > self.accuracy:
                self.accuracy = acc
                self.write_network_model_json(path=write_model_path)
                print("Epoch "+str(j+1) +
                      " : Accuracy have improved. Writing to the File.")
                print()
            else:
                print("Epoch "+str(j+1) +
                      " : Accuracy not satisfying enough to be written")
                print()
        self.history["accuracy"] = accs

    # OK
    def forward(self, i):
        self.Model[0].setInput(self.inputLayer[i])
        # self.Model[-1].setTarget(self.labels[i])
        # self.Model[-2].setTarget(self.labels[i])

        for l in range(0, len(self.Model)):
            hasil = self.Model[l].forward()
            if l < len(self.Model):
                self.Model[l+1].setInput(hasil)

        return self.Model[-1].outputLayer
        # , self.Model[-2].outputLayer
    
    # OK
    # def backward(self):
    #     for l in range(len(self.Model)-1, -1, -1):
    #         bw = self.Model[l].backward()
    #         if l > 0:
    #             self.Model[l-1].setLearningRate(self.learning_rate)
    #             self.Model[l-1].setOutputGrad(bw)

    #OK
    # input berupa gambar yang sudah di load dan ga ada urusan sama inputLayer.  OK
    def predict_single(self, input):
      times = int(time.time()*1000)
      self.Model[0].setInput(input)
      for l in range(0, len(self.Model)):
        hasil = self.Model[l].forward()
        if (l + 1) < len(self.Model):
          self.Model[l+1].setInput(hasil)
      times_end = int(time.time()*1000)
      deltaTimes = times_end-times
      return self.Model[-1].outputLayer, deltaTimes 

    # input masih berurusan sama inputLayer kaena inputnya lebih dari 1   OK
    def predict(self):
      # pb = ProgressBar()
      predictions = []
      times = []
      for i in range(0, len(self.inputLayer)):
        prediction , time = self.predict_single(self.inputLayer[i])
        # pb.render_no_time_no_error(i, len(self.inputLayer))
        predictions.append(prediction)
        times.append(time)
      # pb.render_end_no_time_no_error(i, len(self.inputLayer))

      return predictions, times


    def validation_prediction(self):
        errors = []
        predictions = []
        for i in (self.val_id):
            error, prediction = self.forward(i)
            errors.append(error)
            predictions.append(prediction)

        return errors, predictions

    def validate(self):
        counter = 0
        error, prediction = self.validation_prediction()
        for i in range(0, len(prediction)):
            if np.argmax(prediction[i]) == np.argmax(self.labels[i]):
                counter += 1
        return counter/len(prediction)         # return Accuracy

    # Tahap pertama sebelum training,  input.  #1 OK
    def assign_input(self, path, train=True, validation_size=0.25):
      # Mendefinisikan input layer dan validation input.
      self.inputLayer, self.labels = self.multipath_input_generator(
          path, train)
      self.inputLayer = np.array(self.inputLayer).reshape(-1, 1, 96, 96, 1)
      self.create_validation_input(validation_size)
      self.inputLayer = np.array(self.inputLayer)/255
      
      #definisikan class.
    #   self.num_class = len(self.unique(label))


    def read_network_model_json(self, path):
      self.inputSize = 96
      self.inputDepth = 1

      model = []
      with open(path, "r") as model_file:
        data = js.load(model_file)
        # self.accuracy = data['accuracy']
        for p in data['model']:
          types = p['Layer']['type']
          ps = p['Layer']
          if types == 'Convolution':
            l = ConvolutionLayer(input_size = ps['input_shape'], filters=ps['filters'], kernel_size=ps['kernel_size'], activation = ps['activation'])
            l.setBias(ps['bias'])
            l.setKernel(ps['weight'])
            self.add(l)
          elif types == 'Flatten':
            self.add(FlattenLayer(ps['input_shape']))
          elif types == 'Dense':
            l = FullyConnectedLayer(output_shape = ps['output_shapes'], input_shape = ps['input_shapes'], activation = (ps['activation'] if ps['activation'] != None else 'relu'))
            l.setWeight(ps['kernel'])
            l.setBias(ps['bias'])
            self.add(l)
            self.num_classes = ps['output_shapes'][1]
          elif types == 'Pooling':
            p = PoolingLayer(inputSize = ps['input_shape'], stride = ps['stride'][0], kernel_size = ps['kernelSize'][0])
            self.add(p)
          # elif types == 'Relu':
          #   model.append("Relu")
          # elif types == 'Softmax':
          #   model.append("Softmax")
          else:
              continue
      print("Total Layer : ", len(self.Model))

    def __str__(self):
      self.summary()

    def create_validation_input(self, validation_size=.25):
        input_size = len(self.inputLayer)

        my_list = list(range(1, input_size))  # list of integers from 1 to 99
        # adjust this boundaries to fit your needs
        random.shuffle(my_list)

        val_id = [my_list[i]
                  for i in range(0, int(validation_size*input_size))]

        # val_feature = []
        # val_labels = []
        # for i in res:
        #   val_feature.append(self.inputLayer[i])
        #   val_labels.append(self.labels[i])

        # self.validation_data = val_feature
        # self.validation_labels = val_labels
        self.val_id = val_id
        print("Successfully created " +
              str(int(input_size*validation_size))+" validation data.")

    def create_confusion_matrix_calculate_accuracy(self):
        # Prediction, Target
        print("Num Classes : ", self.num_classes)
        matrix = np.zeros((self.num_classes, self.num_classes))
        print("Confusion Matrix shape :", matrix.shape)
        total = 0
        # tp = np.zeros(self.num_classes)
        # fn = np.zeros(self.num_classes)
        # fp = np.zeros(self.num_classes)
        # tn = np.zeros(self.num_classes)
        tp = 0
        fn = 0
        fp = 0
        tn = 0

        
        # tp = untuk samoyed = pss, pmm, pgg
        # tn = untuk samoyed = pgg+pmg+pgm+pmm
        # fp = kelasnya salah  untuk samoyed = psg + psm
        # fn = predictnya salah  untuk mastiff = pgm + psm (actual = m)
        predictions = []
        for i in range(0, len(self.inputLayer)):
            prediction, _ = self.predict_single(self.inputLayer[i])
            # print(prediction,",",self.labels[i])
            matrix[self.labels[i]][int(np.argmax(prediction))] += 1
            predictions.append(prediction)
            total += 1

        for i in range(0, self.num_classes):        # Actual
            for j in range(0, self.num_classes):    # Predicted
                if i == j:  
                    tp += matrix[i][j]
                elif i != j:
                    fp += matrix[i][j]
                    fn += matrix[j][i]

        for a in range(0, self.num_classes):
            for i in range(0, self.num_classes):        # Actual
                for j in range(0, self.num_classes):    # Predicted
                    if (a != i or a != j) and i==j:
                        tn += matrix[i][j]

        # for i in matrix:
        #     for j in i:
        #         print(int(j), end=" ")
        #     print()

        print("TP : ",tp)
        print("TN : ",tn)
        print("FP : ",fp)
        print("FN : ",fn)

        acc = (tp+tn)/(tp+tn+fp+fn)
        recall = tp/(tp+fn)
        precision = tp/(tp+fp)
        print("Recall : ",recall)
        print("Precision : ",precision)


        return acc, recall, precision
